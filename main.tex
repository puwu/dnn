\documentclass[a4paper,12pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{commath}
\usepackage[short]{optidef}

\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{graphicx}
\usepackage[round]{natbib}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue]{hyperref}
\usepackage[capitalize,nameinlink,noabbrev]{cleveref} % must load after 
%hyperref to work

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}{Proposition}[section]
\newtheorem{remark}{Remark}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{example}{Example}[section]

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\st}{s.t.}
\newcommand*{\T}[1]{{#1{}}^{\mkern-1.5mu\mathsf{T}}}
\newcommand*{\inv}[1]{{#1{}}^{\raisebox{.2ex}{$\scriptscriptstyle-1$}}}
\newcommand*{\ginv}[1]{{#1}^{+}}

\numberwithin{equation}{section}
\newcommand{\keyword}[1]{\textbf{#1}}

\newcounter{problem}
\crefname{problem}{Problem}{Problem}

\newcommand{\comm}[1]{{\color{green} \textbf{Comment:} #1}}
\newcommand{\todo}[1]{{\color{red} \textbf{Todo:} #1}}

\newcommand{\RR}{\mathbb{R}}
\newcommand{\EE}{\mathbb{E}}

\begin{document}

\title{Learning Neural Networks by Back-Propagating Representations}
%\author{Suqi Liu\\
%Princeton University\\
%\texttt{suqi@princeton.edu}}

\maketitle

\section{Introduction}
Back-propagation~\citep{rumelhart1985learning, werbos1990backpropagation} 
has been the dominating strategy for training neural networks.
However, there is a long-standing problem of gradient 
diffusion~\citep{hochreiter1991untersuchungen, hochreiter2001gradient} and 
many remedies have been proposed, including pre-training the 
model~\citep{schmidhuber1992learning}, greedy training~\citep{hinton2006fast} 
and so on.
Here, we rethink the optimization for multilayer fully-connected neural 
networks and try to create procedures that are tailored to the specific problem.
Our result is a partially constrained block coordinate descent method 
that is derivative free.
It also conforms an online update rule that is very similar to the perceptron 
algorithm.
The comparison of the proposed algorithm and the traditional back-propagation 
would be an analogy to that of gradient descent and block coordinate descent.

\section{Projected block coordinate descent}
In this section, we introduce a penalty method for solving neural network 
optimization problem.
For details of penalty method and block coordinate descent, we refer to the 
standard texts by~\citet{luenberger1984linear} 
and by~\citet{bertsekas1999nonlinear}.
For simplicity, we only consider squared loss and quadratic penalty function 
for binary classification problem.

The learning of neural networks can be formulated as the following constrained 
optimization problem:
\begin{mini}
{w, A, x, z}{\sum_i (y_i - x_{iL})^2}{\label[problem]{pb:nn}}{}
\addConstraint{z_{il}}{= {A_l} x_{i(l-1)},\quad}{i = 1, \ldots, n,\quad l = 
1, \ldots, L}
\addConstraint{x_{il}}{= \sigma(z_{il}),\quad}{i = 1, \ldots, n,\quad l = 1, 
\ldots, L}
\end{mini}
where $\{(x_{i0}, y_i)\}$ are the input data.

By employing penalty method, we convert the problem into a \emph{partially 
constrained} optimization using quadratic penalty function, which yields the 
following problem:
\begin{mini}
{w, A, x, z}{\sum_i (y_i - x_{iL})^2 + c \sum_i \sum_l \norm{z_{il} - 
{A_l} x_{i(l-1)}}^2}{\label[problem]{pb:pn}}{}
\addConstraint{x_{il}}{= \sigma(z_{il}),\quad}{i = 1, \ldots, n,\quad l = 1, 
\ldots, L.}
\end{mini}

The constraint set in \cref{pb:pn} can be viewed as a nonlinear 
manifold.

\comm{One interesting aspect of this problem is that the objectives are 
separable, which means that they can be optimized individually.
However, introducing the manifold constraints will result in entangled 
objectives.}

We cycle through the parameters in the order of $z_1, \ldots, z_L, x_L, A_L, 
\ldots, x_1, A_1$ with projection on the manifold.
The optimizing step over $z_1, \ldots, z_L$ is called \emph{forward pass}, 
which simply computes the hidden state values with current $A_l$s.
Alternating between $x_l$ and $A_l$ is called \emph{backword pass}.
In each layer, it solves the following problem
\begin{mini}
{A_{l}, x_{i(l-1)}}{\sum_i \norm{z_{il} - {A_l} 
x_{i(l-1)}}^2}{\label[problem]{pb:sm}}{}
\end{mini}

This problem is actually a matrix completion problem.
A simple approach is alternating minimization, where we minimize the objective 
with respect to $A_{l}$ and then $x_{i(l-1)}$.
This breaks the problem down to two separate linear regressions
\begin{mini}
{A_{l}}{\sum_i \norm{z_{il} - {A_l} 
x_{i(l-1)}}^2}{\label[problem]{pb:sa}}{}
\end{mini}
and
\begin{mini}
{x_{i(l-1)}}{\norm{z_{il} - {A_l} 
x_{i(l-1)}}^2}{\label[problem]{pb:sx}}{}.
\end{mini}

Due to nonlinear activation, the covariance matrix of $x_{l-1}$ is usually 
nonsingular.
(We may have a theorem for that.)
However, if $d_{l-1} > d_l$, \cref{pb:sx} will be under-determined.
We can replace it by the pseudo inverse.
This actually solves the optimization in \cref{ag:pc}.
\begin{mini}
{x}{\norm{x}^2}{\label[problem]{pb:sp}}{}
\addConstraint{z_{il}}{= {A_l} x}
\end{mini}

\comm{
A close form solution for matrix factorization can be found by SVD.
It may be of interest to further investigate this method.
In the following part, we show a fairly general statement that guarantees the 
exact recovery for any solution in the subspace.
}

We introduce \cref{ag:pc} to solve the problem.
The algorithm cyclically minimizes over a set of variables and then project the 
data to the constraint manifold.

\begin{algorithm}[t]
\caption{Projected Block Coordinate Descent for Neural Networks}\label{ag:pc}
\begin{algorithmic}
\REQUIRE $A^0$, $\{(x_{i0}, y_i)\}$, $t = 0$
\REPEAT
\STATE (Forward pass to get $x_{il}^{t}$ and 
$z_{il}^{t}$ with $A_{l}^{t}$ fixed.)
\FOR{$l = 1, \ldots, L$}
\STATE $z_{il}^{t} \leftarrow {A_l^t} x_{i(l-1)}^{t}$ 
(minimizer of $q(v, c)$)
\STATE $x_{il}^{t} \leftarrow \sigma(z_{il}^{t})$ 
(projection onto $\mathcal{M}$)
\ENDFOR
\STATE (Backward pass to update $A_l$.)
\STATE $\tilde{x}_{iL}^{t} \leftarrow y_i$ (minimizer of $q(v, c)$)
\FOR{$l = L, \ldots, 1$}
\STATE $\tilde{z}_{il}^{t} \leftarrow \inv{\sigma} (\tilde{x}_{il}^{t})$ 
(projection onto $\mathcal{M}$)
\STATE $\T{A_{l}^{t+1}} \leftarrow \ginv{\left(\sum_i 
x_{i(l-1)}^{t} \T{x_{i(l-1)}^{t}}\right)}
\sum_i x_{i(l-1)}^{t} \T{\tilde{z}_{il}^t}$ (minimizer of $q(v, c)$)
\STATE $\tilde{x}_{i(l-1)}^{t} \leftarrow \ginv{(A_l^{t+1})}\tilde{z}_{il}^{t}$
\ENDFOR{}
\STATE $t \leftarrow t + 1$
\UNTIL{$P(v) = 0$}
\end{algorithmic}
\end{algorithm}

If we denote the vector of parameters $(w, A, x, z)$ by $v$, the problem can be 
written as
\begin{mini}
{v}{q(v, c) = f(v) + c P(v)}{\label[problem]{pb:pe}}{}
\addConstraint{v}{\in \mathcal{M},}
\end{mini}
where $f(v) = \sum_i {(y_i - x_{iL})}^2$ is the loss function, $P(v) = \sum_i 
\sum_l \norm{z_{il} - {A_l}\T x_{i(l-1)}}^2$ is the quadratic penalty and 
$\mathcal{M}$ is the manifold formed by the activation $x_{il} = 
\sigma(z_{il})$.

\iffalse
\begin{lemma}
If the stop condition is met, i.e. $P(v^*) = 0$, then $v^*$ is a stationary 
point of \cref{pb:nn}.
\end{lemma}

\begin{lemma}
\begin{equation}
\begin{aligned}
q(v^t, c^t) \le q(v^{t+1}, c^{t+1})\\
P(v^t) \ge P(v^{t+1})\\
f(v^t) \le f(v^{t+1})
\end{aligned}
\end{equation}
\end{lemma}

\begin{theorem}
The algorithm converges to a local minimal of the function $f(v)$.
\end{theorem}
\fi

\section{Exact recovery for deep linear neural networks}
The nonlinearity of neural networks comes from two sources:
1.\ the nonlinear activation function;
2.\ the multiplication of weight matrices.
The former can be avoided by using only linear units, which results in a 
simplified structure termed linear feed-forward neural 
networks~\citep{baldi1989neural}.
The pioneering work by~\citet{baldi1989neural} first investigated this 
simplified model.
They concluded that linear feed-forward neural networks have no local minimum 
and every other critical point is a saddle point.
\citet{saxe2013exact} took one step further and demonstrate some common 
phenomena shared by both linear and nonlinear networks.
The conjecture is also shown to be true for nonlinear neural networks by a 
recent work~\citep{kawaguchi2016deep}.
Interestingly, in the end of the paper~\citep{baldi1989neural}, a alternating 
minimization procedure for two layer linear neural networks was mentioned, 
which is very similar to our strategy.
However, the authors gave no clue for how to optimize over nonlinear 
activations.

In the rest of this section, we show that the linear feed-forward neural 
networks can be learned to exactly recover the true model by proposed method 
with random initialization.
Suppose we are given a linear feed-forward neural network as
\begin{equation}
y = f(x) = A_L \ldots A_1 x.
\end{equation}
We consider the squared loss function
\begin{equation}
L(X, Y) = \norm{Y - A_L \ldots A_1 X}_2^2.
\end{equation}

It can be shown that the proposed algorithm converges to optimal in one 
forward-backward pass.
If the matrices $A_i^0$ have i.i.d.\ Gaussian entries, recent progresses in 
matrix perturbation theory~\citep{burda2010eigenvalues,akemann2013products} 
suggests that the product matrices $A_1^0 \ldots A_l^0$ is nonsingular 
with high probability.

Suppose the data is generated by a linear model $y = Cx$, where $C \in \RR^{m 
\times n} (m \le n)$ has full row rank.
Denote $S = \sum_i x_i \T{x_i}$ and assume $S$ is nonsingular.
Let $B_l = A_l \ldots A_1$ and $D_l = A_L \ldots A_l$.
For notational convenience, denote $B_0 = I_n$ and $D_{L+1} = I_m$.
The notation $\ginv{(\cdot)}$ is used to denote the generalized inverse of a 
square matrix.
Throughout the discussion, we assume the width of each hidden layer $d_l \ge 
m$, which is natural for exact recovery.

\begin{lemma}[Stationary condition]\label{lm:sc}
The stationary condition for the deep linear feed-forward neural network is
\begin{equation}\label{eq:st}
B_{l-1} S \T{B_{l-1}} \T{A_l} \T{D_{l+1}} = B_{l-1} S \T{C},\qquad l = 
1, \ldots, L.
\end{equation}
\end{lemma}
\begin{proof}
The square loss of the network is
\begin{equation*}
\begin{split}
L(A_1, \ldots, A_L) &= \sum_i \norm{y_i - A_L \ldots A_1 x_i}^2\\
&= \sum_i \norm{Cx_i - A_L \cdots A_1 x_i}^2\\
&= \sum_i \left(\T{x_i} \T{C} C x_i + \T{x_i} \T{A_1} \cdots \T{A_L} A_L \cdots 
A_1 x_i - 2 \T{x_i} \T{C} A_L \cdots A_1 x_i\right)\\
&= \Tr(\T{C} C S) + \Tr(\T{A_1} \cdots \T{A_L} A_L \cdots A_1 S) - 2 \Tr(\T{C} 
A_L \cdots A_1 S).
\end{split}
\end{equation*}
By first order necessary condition, we have
\begin{equation*}
\frac{\partial L}{\partial A_l} = 2 A_{l-1} \cdots A_1 S \T{A_1} \cdots \T{A_L} 
A_L \cdots A_{l+1} - 2 A_{l-1} \cdots A_1 S \T{C} A_L \cdots A_{l+1} = 0,
\end{equation*}
which gives
\begin{equation*}
B_{l-1} S \T{B_{l-1}} \T{A_l} \T{D_{l+1}} D_{l+1} = B_{l-1} S \T{C} D_{l+1},
\end{equation*}
Since $D_{l+1}$ has full row rank,
\begin{equation*}
B_{l-1} S \T{B_{l-1}} \T{A_l} \T{D_{l+1}} = B_{l-1} S \T{C}.
\end{equation*}
\end{proof}
Note that there are other ways to express the stationary condition with $B$ and 
$D$.
We choose this form for the simplicity of the following algorithms and proofs.

\begin{lemma}\label{lm:fr}
If the entries of matrices $A_l$ are initialized with Gaussian random 
variables, with high probability, $B_l$s has full rank.
\end{lemma}
This directly follows standard results in random matrix theory.
By full rank we mean full row or column rank, whichever is smaller.

\begin{lemma}[Backward fitting]\label{lm:bf}
If $A_l$s are found in the backward order ($l = L, \ldots, 1$), such that 
each $A_l$ satisfies the $l$th stationary condition with all others fixed and 
has rank no smaller than $m$, then the network perfectly recovers the true 
model.
\end{lemma}

\begin{proof}
Suppose $k$ is the first hidden layer whose width is smaller than $n$.
Then $B_{k-1}$ has rank greater than or equal to $n$.
By multiplying $\T{B_{k-1}}$ to the left of both sides of the $k$th stationary 
condition, we have
\begin{equation*}
\T{B_{k-1}} B_{k-1} S \T{B_{k-1}} \T{D_k} = \T{B_{k-1}} 
B_{k-1} S \T{C}.
\end{equation*}
Since $\T{B_{k-1}} B_{k-1}$ and $S$ are nonsingular, we 
have
\begin{equation*}
D_k B_{k-1} = C,
\end{equation*}
where all matrices in $D_k$ satisfy the stationary condition and all matrices 
in $B_{k-1}$ are random Gaussian.
This implies the backward procedure stops at $k$th step and recovers the true 
model.
\end{proof}

\comm{Actually perfectly recovery for noiseless case can be obtained by 
stationary point of the $k$th condition with respect to $A_k$, which can be 
solved by gradient descent.}

\begin{lemma}\label{lm:ss}
$A_l$ and $x_{il}$ found by~\cref{ag:pc} satisfy
\begin{equation}\label{eq:sa}
B_{l} S \T{B_{l}} \T{A_{l+1}} = B_{l} \sum_i x_i \T{x_{l+1}}
\end{equation}
and
\begin{equation}\label{eq:sx}
\T{x_{il}} \T{A_{l+1}} A_{l+1} = \T{x_{i(l+1)}} A_{l+1}.
\end{equation}
\end{lemma}

\begin{proof}
The loss can be written as
\begin{equation*}
\begin{split}
L(A_{i(l+1)}, x_{il}) &= \sum_i \norm{x_{i(l+1)} - A_{i(l+1)} x_{il}}^2\\
&= \sum_i \Tr(x_{i(l+1)} \T{x_{i(l+1)}}) - 2\Tr(\T{x_{i(l+1)}} A_{i(l+1)} 
x_{il}) + \Tr(\T{x_{il}} \T{A_{i(l+1)}} A_{i(l+1)} x_{il}).
\end{split}
\end{equation*}
By taking the derivatives and set them to $0$, we have
\begin{equation*}
\begin{aligned}
\sum_i x_{il} \T{x_{il}} \T{A_{i(l+1)}} &= \sum_i x_{il} \T{x_{i(l+1)}}\\
\T{x_{il}} \T{A_{i(l+1)}} A_{i(l+1)} &= \T{x_{i(l+1)}} A_{i(l+1)}.
\end{aligned}
\end{equation*}
Since $x_{il} = B_l x_i$, the first condition can also be written as
\begin{equation*}
B_{l} S \T{B_{l}} \T{A_{l+1}} = B_{l} \sum_i x_i \T{x_{l+1}}.
\end{equation*}
\end{proof}

\begin{lemma}\label{lm:hv}
The hidden values found by~\cref{ag:pc} in the backward pass satisfies
\begin{equation}
\sum x_i \T{x_{il}} \T{D_{l+1}} = S \T{C}
\end{equation}
where all matrices in $B_{l-1}$ are from initialization and all those in 
${D_{l+1}}$ are from backward fitting.
\end{lemma}

\begin{proof}
We prove it by backward induction.

For the last layer, which is the output, the minimizer gives $x_{iL} = z_{iL} = 
y_i = Cx_i$.
Then $\sum_i x_i \T{x_{iL}} = \sum_i x_i \T{x_i} \T{C}$.

Suppose for $(l+1)$th layer, the assumption holds, i.e.\ $\sum_i x_i 
\T{x_{i(l+1)}} \T{D_{l+2}} = S \T{C}$.
Therefore, we have
\begin{equation}
\sum_i x_i \T{x_{i(l+1)}} = S \T{C} \inv{\left(D_{l+2} \T{D_{l+2}}\right)} 
D_{l+2}.
\end{equation}
Using \cref{eq:sa}, we have
\begin{equation*}
\begin{split}
\T{A_{l+1}} = \ginv{\left(B_l S \T{B_l}\right)} B_l S \T{C} \inv{\left(D_{l+2} 
\T{D_{l+2}}\right)} D_{l+2}.
\end{split}
\end{equation*}
By~\cref{eq:sx},
\begin{equation*}
\sum_i x_i \T{x_{il}} \T{A_{i(l+1)}} A_{i(l+1)} = \sum_i x_i \T{x_{i(l+1)}} 
A_{i(l+1)},
\end{equation*}
which give,
\begin{equation*}
\begin{split}
\sum_i x_i \T{x_{il}} \ginv{\left(B_l S \T{B_l}\right)} B_l S \T{C} 
\inv{\left(D_{l+2} \T{D_{l+2}}\right)} C S \T{B_l} \ginv{\left(B_l S 
\T{B_l}\right)}\\ = S \T{C} \inv{\left(D_{l+2} \T{D_{l+2}}\right)} 
C S \T{B_l} \ginv{\left(B_l S \T{B_l}\right)},
\end{split}
\end{equation*}
that is
\begin{equation*}
\sum_i x_i \T{x_{il}} \ginv{\left(B_l S \T{B_l}\right)} B_l S \T{C} = S \T{C}.
\end{equation*}
Hence,
\begin{equation*}
\sum_i x_i \T{x_{il}} \T{D_{l+1}} = S \T{C}.
\end{equation*}
\end{proof}

\begin{lemma}\label{lm:pc}
Each $A_i$ found by~\cref{ag:pc} in one backward pass satisfies the $i$th 
stationary condition.
\end{lemma}

\begin{proof}
By~\cref{lm:hv} and~\cref{eq:sa},
\begin{equation*}
B_{l-1} S \T{B_{l-1}} \T{A_{l}} \T{D_{l+1}} = B_{l-1} \sum_i x_i \T{x_{l}} 
\T{D_{l+1}} = B_{l-1} S \T{C},
\end{equation*}
which is exactly the $i$th stationary condition in~\cref{eq:st}.
\end{proof}

By combining \crefrange{lm:sc}{lm:pc}, we have the following theorem.
\begin{theorem}
Linear feed-forward neural network learned by \cref{ag:pc} perfectly recovers 
the true linear model.
\end{theorem}

\section{Convergence for ReLU activation}
ReLU~\citep{nair2010rectified} has received wide success in practice.
Recently, there have been many results on the convergence for two-layer neural 
networks with ReLU 
activation~\citep{zhong2017recovery,tian2017analytical,li2017convergence,ge2017learning}.
However, all of the analyses only work for the over-simplified two-layer 
structure.
Also, gradient based framework is the only setting under consideration.
We try to fill this gap by using the proposed algorithm.

At first glance, ReLU does not fit into our scheme since it is not invertible.
However, a close inspection show that ReLU can actually be viewed as a 
projection step, where all the positive entries are observed while negative 
entries are unobserved.
Based on this observation, we can reformulate the backward pass as a matrix 
completion problem.

For the ease of notation, we rewrite the neural network learning in a matrix 
form.

\begin{mini}
{A, X}{\norm{Y - X_L}_F^2}{\label[problem]{pb:mn}}{}
\addConstraint{X_{l}}{= P_{\Omega_l}(A_l X_{l-1}),\quad}{l = 1, \ldots, L}
\end{mini}


\begin{algorithm}[t]
\caption{ReLU Networks}\label{ag:pm}
\begin{algorithmic}
\REQUIRE $A^0$, $X_0, Y$, $t = 0$
\REPEAT
\FOR{$l = 1, \ldots, L$}
\STATE $X_{l}^t \leftarrow P_{\Omega_l^t}({A_l^t} X_{l-1}^t)$ 
\ENDFOR
\STATE $\widetilde{X}_{L}^{t} \leftarrow Y$
\FOR{$l = L, \ldots, 1$}
\STATE $A_l^{t+1} \leftarrow \argmin_{A_l} 
\norm{P_{\Omega_l^t}(\widetilde{X}_{l}^t - A_l X_{l-1}^t)}_F^2$
\STATE $\widetilde{X}_{l-1}^{t} \leftarrow \argmin_{X_{l-1}} 
\norm{P_{\Omega_l^t}(\widetilde{X}_{l}^t - A_l^{t+1} X_{l-1})}_F^2$
\ENDFOR{}
\STATE $t \leftarrow t + 1$
\UNTIL{$P(v) = 0$}
\end{algorithmic}
\end{algorithm}

We show that \ref{ag:pm} has local linear convergence for the models considered 
in~\cite{tian2017analytical,li2017convergence}.
Actually we are considering a much more complex model.
Note that the models they solved is actually convex.

\begin{lemma}[Subadditivity of ReLU]\label{lm:subadd}
Let $\sigma(x) = \max(0, x)$ be the ReLU activation function. Then,
\begin{equation*}
\sigma(x + y) \le \sigma(x) + \sigma(y).
\end{equation*}
\end{lemma}

\begin{proof}
Since $\sigma$ is convex,
\begin{equation*}
\sigma(\frac{x + y}{2}) \le \frac{1}{2}(\sigma(x) + \sigma(y)).
\end{equation*}
Additionally, $\sigma$ is homogeneous, i.e. $\sigma(ax) = a\sigma(x)$,
\begin{equation*}
\sigma(x + y) \le \sigma(x) + \sigma(y).
\end{equation*}
\end{proof}

\begin{lemma}\label{lem:condexp}
If $x \sim N(0, I)$, for any vector $a$,
\begin{equation*}
\EE[x\T{x} | \T{a} x \ge 0] = I.
\end{equation*}
\end{lemma}

\begin{proof}
By symmetry of standard normal, $\EE[x\T{x} | \T{a} x \ge 0] = \EE[x\T{x} | 
\T{a} x < 0]$.
Therefore, $\EE[x\T{x} | \T{a} x \ge 0] = \EE[x\T{x}] = I$.
\end{proof}

\begin{lemma}\label{lem:triagineq}
Let $a \in \RR^p$ and $b \in \RR^p$ be unit normal vectors of two planes, 
i.e.\ $\norm{a}_2 = 1$ and $\norm{b}_2 = 1$.
Denote the angle between the two planes by $\theta$.
Then, the following bound holds for $\theta$.
\begin{equation}
\theta \le \left\lbrace \begin{aligned}
&2\norm{a + b}, \quad &a \cdot b < 0\\
&\frac{\pi}{4}\norm{a + b}^2, \quad &otherwise.
\end{aligned}\right.
\end{equation}
Or equivalently, for any $\theta \in [0, \pi]$,
\begin{equation}
\theta \le \left\lbrace \begin{aligned}
&2\sqrt{1 - \cos \theta}, \quad &\theta \in [0, \pi/2) \\
&\frac{\pi}{2}(1 - \cos \theta), \quad &otherwise.
\end{aligned}\right.
\end{equation}
\end{lemma}

\begin{proof}
For $\theta \in [0, \pi/2)$,
\begin{equation*}
\theta = 2 \cdot \frac{\theta}{2} \le 2 \tan \frac{\theta}{2} = 2\sqrt{\frac{1 
- \cos \theta}{1 + \cos \theta}} \le 2 \sqrt{1 - \cos \theta}.
\end{equation*}
For $\theta \in [\pi/2, \pi]$, let $f(\theta) = \frac{\pi}{2}(1 - \cos \theta) 
- \theta$.
Then, $\frac{d^2 f}{d \theta^2} = \cos \theta < 0$ for $\theta \in (\pi/2, 
\pi).$
Therefore $f(\theta)$ is concave on $(\pi/2, \pi)$.
Hence, $f(\theta) \ge \max \{f(\pi/2), f(\pi)\} = 0$.
That is, $\theta \le \frac{\pi}{2}(1 - \cos \theta)$.

For the normal vectors of two planes, $\cos \theta = - a \cdot b$.
By plugging in, we have the inequality.
\end{proof}

\begin{lemma}
For a two-layer ReLU network with fixed last layer, if the $x$ are standard 
normal distributed, the squared loss is \emph{strongly convex} in the 
parameters of the first layer.
\end{lemma}

\begin{proof}
Let $f_A(x)$ be the output of the network.
Without loss of generality, assume the weights of last layer $w \in {\{-1, 
1\}}^m$, where $m$ is the number of hidden units.
Denote each row vector of $A$ by $a_j$.
Then,
\begin{equation*}
f_A(x) = \sum_{j=1}^{m} w_j \sigma(\T{a_j} x).
\end{equation*}
The squared loss is defined as
\begin{equation*}
\mathcal{L}(A) = \EE_{(x, y) \sim \mathcal{D}} \norm{y - f_A(x)}^2.
\end{equation*}
\begin{equation*}
\frac{\partial \mathcal{L}}{\partial a_j} = \EE_{(x, y) \sim \mathcal{D}} 
[-2(y - \sum_{j=1}^{m} w_j \sigma(\T{a_j} x))\sigma'(\T{a_j} x)\T{x}]
\end{equation*}
\begin{equation*}
\begin{aligned}
\frac{\partial^2 \mathcal{L}}{\partial {a_j}^2} &= \EE_{(x, y) \sim 
\mathcal{D}} [2\sigma'(\T{a_j} x)^2 x\T{x}]\\
&= 2 \EE_{(x, y) \sim \mathcal{D}} 
[x\T{x} | \T{a_j} x \ge 0] \Pr(\T{a_j} x \ge 0)\\
\frac{\partial^2 \mathcal{L}}{\partial {a_j}\partial {a_k}} &= \EE_{(x, y) \sim 
\mathcal{D}} [2 w_i w_j \sigma'(\T{a_j} x)\sigma'(\T{a_k} x) x\T{x}]\\
&= 2 w_i w_j\EE_{(x, y) \sim \mathcal{D}} [x\T{x} | \T{a_j} x \ge 0, \T{a_k} x 
\ge 0] \Pr(\T{a_j} x \ge 0, \T{a_k} x \ge 0).
\end{aligned}
\end{equation*}
By Lemma~\ref{lem:condexp}, $\EE_{(x, y) \sim \mathcal{D}} [x\T{x} | \T{a_j} x 
\ge 0, \T{a_k} x \ge 0] = \EE_{(x, y) \sim \mathcal{D}} [x\T{x} | \T{a_j} x \ge 
0] = I$.
$\Pr(\T{a_j} x \ge 0) = \frac{1}{2}$ and if $a_j$s are Gaussian random vectors, 
$\Pr(\T{a_j} x \ge 0, \T{a_k} x \ge 0) = \frac{1}{4}$.
Therefore,
\begin{equation*}
\nabla^2 \mathcal{L} = \frac{1}{2} I_{m \times p} + w\T{w} \otimes I,
\end{equation*}
where $\otimes$ is the Kronecker product.

For arbitrary $A$, define 
\begin{equation*}
K(a, b) = 1 - \frac{1}{\pi} \arccos \frac{a \cdot b}{\norm{a}\norm{b}}.
\end{equation*}
This is exactly the same as the $\arccos$ kernel introduced 
by~\citet{cho2009kernel}.
Let $K$ be the matrix where $K_{jk} = K(w_j a_j, w_k a_k)$.
Then,
\begin{equation*}
\nabla^2 \mathcal{L} = K \otimes I.
\end{equation*}
Since $K$ is positive semidefinite~(PSD), by the property of Kronecker product, 
$\nabla^2 \mathcal{L}$ is also PSD.
Hence, the objective is convex.
\end{proof}

\begin{theorem}\label{thm:two}
If we have a ReLU network with the weights of the last layer fixed, then 
\ref{ag:pm} converges linearly with a good initial value.
\end{theorem}

\begin{proof}
Suppose we have a network with the weights of last layer $w$ fixed.
Without loss of generality, we assume $\norm{w}_2 = 1$.
We further suppose that the data is generated by $y = \T{w}\sigma(A^*x)$.
Our goal is to recover $A^*$.
Since there is only one hidden layer, we drop the layer index $l$ without 
causing ambiguity.
According to the update rule, at $t$th backward pass,
\begin{equation*}
\begin{split}
\tilde{x}_i^t &= x_i^t + w\inv{(\T{w}w)}(y - \T{w} x_i^t)\\
&= x_i^t + w(y_i - \T{w} x_i^t).
\end{split}
\end{equation*}
Let $\Omega_j^t$ be the index set where $x_i^t > 0$, i.e. $\Omega_j^t = \{i: 
A_j^t x_i > 0\}$, where $A_j$ is the $j$th column of $A$.
Then,
\begin{equation*}
\begin{split}
A_j^{t+1} &= \inv{\left(\sum_{i \in \Omega_j^t} x_i \T{x_i}\right)} \sum_{i \in 
\Omega_j^t} \tilde{x}_{ij}^t \T{x_i}\\
&= \inv{\left(\sum_{i \in \Omega_j^t} x_i \T{x_i}\right)} \sum_{i \in 
\Omega_j^t} (A_j^t x_i + w_j(y_i - \T{w} A^t x_i)) \T{x_i}\\
&= A_j^t - w_j \T{w} A^t + w_j \inv{\left(\sum_{i \in \Omega_j^t} x_i 
\T{x_i}\right)} \sum_{i \in \Omega_j^t} y_i \T{x_i}.
\end{split}
\end{equation*}
By~Lemma, $\Omega_j^t = \Omega_j^{t-1}$, we have
\begin{equation}
A^{t+1} - A^t = \left(I - w \T{w}\right) \left(A^{t} - A^{t-1}\right).
\end{equation}
\end{proof}

\section{Online update}
\cref{ag:pc} can also be adapted to use online update, where each data point is 
presented sequentially.
The forward pass and projection step remain the same, while the update for 
$A_l$ changed to block coordinate gradient descent version.
Instead of solving the optimization problem exactly using all data, each $A_l$ 
is updated by subtracting the gradient at current step multiplied by a learning 
rate $\eta$.
The update for $x$ and $z$ in backward pass can be computed in the same way as 
batch version.
This results in an online version as in~\cref{ag:op}.

\begin{algorithm}[t]
\caption{Online Projected Block Coordinate Descent for Neural 
Networks}\label{ag:op}
\begin{algorithmic}
\REQUIRE Initial values $A_l^0$
\FOR{$t = 1, \ldots, T$}
\STATE Observe $(x^t, y^t)$ and set $x_{0} = x^t, y = y^t$
\STATE (Forward pass to get $x_{l}$ and 
$z_{l}$ with $A_{l}^{t}$ fixed.)
\FOR{$l = 1, \ldots, L$}
\STATE $z_{l} \leftarrow {A_l^t} x_{l-1}$
\STATE $x_{l} \leftarrow \sigma(z_{l})$
\ENDFOR
\STATE (Backward pass to update $A_l$.)
\STATE $\tilde{x}_{L} \leftarrow y$
\FOR{$l = L, \ldots, 1$}
\STATE $\tilde{z}_{l} \leftarrow \inv{\sigma} (\tilde{x}_{l})$
\STATE $\T{A_{l}^{t+1}} \leftarrow \T{A_{l}^{t}} + \eta 
x_{l-1}\T{(\tilde{z}_l - z_l)}$
\STATE $\tilde{x}_{l-1} \leftarrow \ginv{A_l^{t+1}} \tilde{z}_l$
\ENDFOR{}
\ENDFOR{}
\end{algorithmic}
\end{algorithm}

\comm{
\citet{strehl2008online} proposed an online linear regression algorithm with 
sample complexity bound $\tilde{O}(n^3/\epsilon^4)$.
We may consider similar schemes in the update for $A_l$ to achieve theoretical 
guarantees.
}

\section{Convergence analysis}

In this section, we consider the general case where the activation is nonlinear.
Despite the non-convexity, most practically used activation functions, such as 
sigmoid, $\tanh$ and ReLU, have some common properties.
In essence, they are merely perturbed version of identity function, which means 
they are closely enough to an identical transform.
We characterize the characteristics by several assumptions.
Based on those general enough assumptions, we show the convergence of the 
proposed algorithm.


\bibliography{ref}
\bibliographystyle{plainnat}
\end{document}
