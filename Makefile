# Makefile for xelatex
NAME=main

all: pdf clean

pdf: $(NAME).tex
	xelatex -interaction=nonstopmode -file-line-error $(NAME).tex
	bibtex $(NAME)
	xelatex -interaction=nonstopmode -file-line-error $(NAME).tex 
	xelatex -interaction=nonstopmode -file-line-error $(NAME).tex 

clean:
	rm -f *.aux *.log *.out *.toc *.blg *.bbl *.synctex.gz

clear: clean
	rm -f *.pdf
